import java.util.List;

public class CallStartedEvent implements IEvent {
    private int callId;

    public CallStartedEvent(int callId) {
        this.callId = callId;
    }

    @Override
    public void run() {
    List<ICallListener> list = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallListener.class);
        for (ICallListener iCallListener : list) {
            iCallListener.callStarted(callId);
        }

    }
}
