public class PhoneApplication implements ICallListener{
    public PhoneApplication() {
        EventDispatcher.instance.registerObject(this);
    }

    @Override
    public void callStarted(int callId) {
        System.out.println("PA: call ended "+callId);
    }

    @Override
    public void callEnded(int callId) {
        System.out.println("PA: call ended "+callId);
    }
}
