public class CallRecorderApplication implements ICallListener{
    public CallRecorderApplication() {
        EventDispatcher.instance.registerObject(this);
    }

    @Override
    public void callStarted(int callId) {
        System.out.println("CRA: call started "+callId);
    }

    @Override
    public void callEnded(int callId) {
        System.out.println("CRA: call ended "+callId);
    }
}
