import java.util.ArrayList;
import java.util.List;

public class AndroidSystem implements ICallListener{
    private List<Object> objectList;

    public AndroidSystem() {
        EventDispatcher.instance.registerObject(this);
        new CallRecorderApplication();
        new PhoneApplication();
        this.objectList = new ArrayList<>();
    }

    @Override
    public void callStarted(int callId) {
        System.out.println("AS: call started "+callId);
    }

    @Override
    public void callEnded(int callId) {
        System.out.println("SA: call ended "+callId);
    }

}
