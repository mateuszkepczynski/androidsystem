import java.util.List;

public class CallEndedEvent implements IEvent {
    private int callId;

    public CallEndedEvent(int callId) {
        this.callId = callId;
    }

    @Override
    public void run() {
        List<ICallListener> list = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallListener.class);
        for (ICallListener iCallListener : list) {
            iCallListener.callEnded(callId);
        }

    }
}
